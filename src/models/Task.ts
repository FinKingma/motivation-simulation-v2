export class Task {
    name: String
    internalization:number = 1
    interest:number
    motivation:number
    blockades:number
    active:boolean
  
    constructor(name: String, interest:number, blockades:number) {
      this.name = name
      this.interest = interest
      this.motivation = interest
      this.blockades = blockades
      this.active = false
    }
  }
  