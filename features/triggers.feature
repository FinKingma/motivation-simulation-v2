@triggers
Feature: Triggers

  Scenario: When I use an external trigger on a task, his drive should increase, but his task internalisation should decrease
    Given My drive is 70
    And The task 'drawing' has an internalization of 0.8
    When I use an external trigger on 'drawing'
    Then My drive should be greater than 70
    And The internalization of 'drawing' should be less than 0.8

  Scenario: When I use an external trigger on a task, it will be activated if his drive is higher than his main blockades + task blockades (if he already had an active task, that one will become inactive)
    Given I have a motivation with 70 drive and 30 blockades
    And The task 'drawing' has a blockade of 30
    When I use an external trigger on 'drawing'
    Then The task 'drawing' should become active

  Scenario Outline: The strength of the external trigger will influence the effect on my drive and task internalisation
    Given My drive is 50
    And The task 'drawing' has an internalization of 0.8
    When I use an external trigger of <strength> on 'drawing'
    Then The internalization of 'drawing' should be <new internalization>
    And My drive should be <new drive>

    Examples:
      | strength | new internalization | new drive |
      | 100      | 0.0                 | 100       |
      | 10       | 0.7                 | 60        |

  @ToDo
  Scenario: When using multiple external triggers subsequently, the effect will decrease

  @ToDo
  Scenario: By using external triggers unrelated to any task, you can increase his drive without any negative effect. The task with the highest motivation will become active in this case
