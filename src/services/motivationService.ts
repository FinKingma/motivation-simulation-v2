import { Motivation } from '../models/Motivation';
import { Task } from '../models/Task';

export class MotivationService {
  motivation:Motivation

  constructor (drive:number, blockades:number) {
    this.motivation = new Motivation(drive, blockades)
  }

  start() {
    setInterval(() => {
      this.updateTasks()
    }, 25)
  }

  stop() {
    clearInterval()
  }

  initializeTask(taskName:String, interest:number, blockades:number) {
      let task:Task = new Task(taskName, interest, blockades)
      this.motivation.tasks.push(task)
  }

  updateTasks() {
    for (let task of this.motivation.tasks) {
      task.motivation = task.interest * task.internalization
    }
  }

  trigger(strength:number, taskName:String) {
    this.motivation.drive += strength
    if (this.motivation.drive > 100) {
      this.motivation.drive = 100
    }
    for (let task of this.motivation.tasks) {
      if (task.name === taskName) {
        if (this.motivation.drive > this.motivation.blockades +task.blockades) {
          task.active = true
        }

        task.internalization -= strength / 100
        if (task.internalization < 0) {
          task.internalization = 0
        }
      }
    }
    this.updateTasks()
  }
}
