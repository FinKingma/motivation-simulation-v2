Motivation Simulation v2

# Model
### Personal Motivation
	int drive (0 - 100)
	int blockades (0 - 100)
	List<Task> tasks
	int activatedTask (name of the task that I am currently doing, or null)

### Task
	String taskName
	int interest (0 - 100)
	int motivation (interest * internalisation)
	int blockades (0 - 100)
	int internalisation (0 - 1)

# Explanations
- Personal motivation blockades can be anything that would prevent you from picking up any kind of task (tired, not enough time, already working on something else, depressed, lost a cat)
- Task blockades can be anything that prevents you from starting (don’t have the tools, task will take much time, already doing something else)
- Task interest is very personal and will not change over time
- Strength of the external trigger is related to the level of internalisation (very strong = external, quite strong = introjected, mild = identified, subtle = integrated)

# Requirements

### Feature: Triggers. I can influence someones motivation through external triggers:
- When I use an external trigger on a task, his drive should increase, but his task internalisation should decrease
- When I use an external trigger on a task, it will be activated if his drive is higher than his main blockades + task blockades (if he already had an active task, that one will become inactive)
- The strength of the external trigger will influence the effect on my drive and task internalisation
- When using multiple external triggers subsequently, the effect will decrease
- By using external triggers unrelated to any task, you can increase his drive without any negative effect. The task with the highest motivation will become active in this case

### Feature: Sustainers. When not interfering with someones motivation, he might be able to sustain his motivation himself
- If his drive drops below the main blockades + active task blockades, the task will no longer be active
- While not using triggers on the current task, the internalisation of that task should slowly increase
- If his current task motivation strength is lower than his drive, his drive should slowly decrease over time
- If his current task motivation strength is higher than his drive, his drive should slowly increase over time, but only if that task is activated
- If he has no active task, his drive will slowly deteriorate


# Goals
1. Start doing task X
2. Maintain a sustained motivation for task Y
3. Maintain a sustained motivation for task Z - which starts with internalisation 0
