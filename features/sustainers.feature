@sustainers
Feature: Sustainers

    @ToDo
    Scenario: If his drive drops below the main blockades + active task blockades, the task will no longer be inactive

    @ToDo
    Scenario: While not using triggers on the current task, the internalisation of that task should slowly increase

    @ToDo
    Scenario: If his current task motivation strength is lower than his drive, his drive should slowly decrease over time

    @ToDo
    Scenario: If his current task motivation strength is higher than his drive, his drive should slowly increase over time, but only if that task is activated

    @ToDo
    Scenario: If he has no active task, his drive will slowly deteriorate
