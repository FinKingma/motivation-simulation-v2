import { expect } from 'chai';
import { AfterAll, Given, When, Then, After, BeforeAll } from 'cucumber'
import { MotivationService } from '../../src/services/motivationService'

let motivationService:MotivationService;

Given('My drive is {int}', function (drive:number) {
  motivationService = new MotivationService(drive,20)
});

Given('I have a motivation with {int} drive and {int} blockades', function (drive:number, blockade:number){
  motivationService = new MotivationService(drive,blockade)
})

Given('The task {string} has an internalization of {float}', function (taskName:String, internalization:number) {
  motivationService.initializeTask(taskName, internalization, 0)
  motivationService.motivation.tasks[0].internalization = internalization
});

Given('The task {string} has a blockade of {int}', function (taskName:String, internalization:number) {
  motivationService.initializeTask(taskName, internalization, 0)
});

When('I use an external trigger on {string}', function(taskName:String) {
  motivationService.trigger(100, taskName)
})

When('I use an external trigger of {int} on {string}', function (strength: number, taskName:String) {
  motivationService.trigger(strength, taskName)
});

Then('My drive should be greater than {int}', function (driveValue:number) {
  expect(motivationService.motivation.drive).to.greaterThan(driveValue);
});

Then('The internalization of {string} should be less than {float}', function (taskName:String, internalization:number) {
  for (let task of motivationService.motivation.tasks) {
    if (task.name === taskName) {
      expect(task.internalization).to.lessThan(internalization);
      return
    }
  }
  throw Error('task not found')
});

Then('The internalization of {string} should be {float}', function (taskName:String, internalization:number) {
  for (let task of motivationService.motivation.tasks) {
    if (task.name === taskName) {
      expect(task.internalization).to.closeTo(internalization, 0.001);
      return
    }
  }
  throw Error('task not found')
});

Then('My drive should be {int}', function (drive) {
  expect(motivationService.motivation.drive).to.be.equal(drive)
});

Then('The task {string} should become active', function(taskName:String) {
  for (let task of motivationService.motivation.tasks) {
    if (task.name === taskName) {
      expect(task.active).to.be.true
    }
  }
})

AfterAll(() => {
  motivationService.stop()
});
