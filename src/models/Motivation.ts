import { Task } from './Task';

export class Motivation {
  tasks:Task[] = []
  drive:number
  blockades:number
  max:number = 100

  constructor(drive:number, blockades:number) {
    this.drive = drive
    this.blockades = blockades
  }
}
