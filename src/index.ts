import Vue from "vue";
import Simulation from "./components/Simulation.vue";

let v = new Vue({
    el: "#app",
    template: `
        <simulation />
    `,
    components: {
        Simulation
    }
});
